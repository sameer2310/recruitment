from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class register(models.Model):
    user = models.OneToOneField(User,on_delete = models.CASCADE)
    phone_no = models.IntegerField()
    email = models.EmailField()
    address = models.TextField(null=True)
    profile_pic = models.ImageField(upload_to = 'images/%y/%m/%d',blank=True)
    registered_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username 



class company(models.Model):
    user1 = models.OneToOneField(User,on_delete=models.CASCADE,null=True)
    contact_no = models.IntegerField()
    city = models.CharField(max_length=100,null=True)
    address = models.TextField(null=True)
 

    def __str__(self):
        return self.user1.username 
      
class postjob(models.Model):
    comp = models.ForeignKey(company,on_delete=models.CASCADE,null=True)
    HR_name = models.TextField()
    Email = models.EmailField()
    contact_no = models.IntegerField()
    job_title = models.TextField()
    job_category = models.TextField()
    vacancies = models.IntegerField()
    qualification = models.TextField()
    job_location = models.TextField()
    interview_details = models.TextField()
    candidate_exp = models.IntegerField()
    salary = models.IntegerField()
    job_desc = models.TextField()

    def __str__(self):
        return str(self.comp )


class apply(models.Model):
    job = models.ForeignKey(postjob,on_delete=models.CASCADE,null=True)
    Comp1 = models.ForeignKey(company,on_delete=models.CASCADE,null=True)
    reg = models.ForeignKey(register,on_delete=models.CASCADE,null=True)
    qualification = models.CharField(max_length=100,null=True)
    Graduation_percentage = models.IntegerField(null=True)
    passing_out_year = models.IntegerField()
    Resume = models.FileField(upload_to='RESUME/%Y/%m/%d')
    experience= models.CharField(max_length=5)
    add_info = models.TextField(null=True)
    Apply_on = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.reg.user.username
    
class contact(models.Model):
    first_name = models.TextField()
    last_name = models.TextField()
    Email_address = models.EmailField()
    mobile_no = models.IntegerField()
    Message = models.TextField(null=True)

    def __str__(self):
        return self.first_name
    









