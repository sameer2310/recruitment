# Generated by Django 2.1.7 on 2019-05-07 05:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recruitment', '0014_contact_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='register',
            name='email',
            field=models.EmailField(default=3, max_length=254),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='register',
            name='address',
            field=models.TextField(null=True),
        ),
    ]
