from django.urls import path,include
from recruitment import views

app_name='recruitment'

urlpatterns=[
    path('about/',views.about,name='about'),
    path('contactus/',views.contactus,name='contactus'),
    path('signup/',views.signup,name='signup'),
    path('usLogin/',views.usLogin,name='usLogin'),
    path('dashboard/',views.dashboard,name='dashboard'),
    path('logout/',views.uslogout,name='uslogout'),
    path('company/',views.companyView,name='company'),
    path('change_password/',views.change_password,name='change_password'),
    path('change_profile/',views.change_profile,name='change_profile'),
    path('compdashboard/',views.compdashboard,name='compdashboard'),
    path('changeprofile/',views.cpprofile,name='changeprofile'),
    path('changepassword/',views.cppassword,name='changepassword'),
    path('postjobs/',views.pjobs,name='postjobs'),
    path('getpost/',views.getpost,name='get_post'),
    path('applyform/',views.aform,name='applyform'),
    path('deleteView/',views.deleteView,name='deleteView'),
    path('deletecp/',views.deletecp,name='deletecp'),
    path('deleteus/',views.deleteus,name='deleteus'),
    path('updatepost/',views.updatept,name='updatepost'),
    path('sendmail/',views.sendmail,name='sendmail'),
    path('candidt/',views.candidatelist,name='candidt'),
    path('cmail/',views.smail,name='cmail'),
    path('forgot_password/',views.forgot_password,name='forgot_password'),
    path('can_delete/',views.can_delete,name='can_delete'),
    path('forgot_pass/',views.forgot_pass,name='forgot_pass'),
   # path('abc',views.uslogin,name='login'),
   # path('accountprofile',views.home,name='home'),
   # path('logout1',views.logout,name='logout'),
   # path('auth1',include('social.djano.urls',namespace='social')),
   
]