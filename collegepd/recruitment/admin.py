from django.contrib import admin
from recruitment.models import register
from recruitment.models import company
from recruitment.models import postjob
from recruitment.models import apply
from recruitment.models import contact

# Register your models here.
admin.site.register(register)
admin.site.register(company)
admin.site.register(postjob)
admin.site.register(apply)
admin.site.register(contact)
