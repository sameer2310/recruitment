from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponseRedirect
from recruitment.models import register
from recruitment.models import company
from recruitment.models import postjob
from recruitment.models import apply
from recruitment.models import contact
import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import check_password
from django.core.mail import EmailMessage

# Create your views here.
def home(request):
    if 'username' in request.COOKIES:
        un = request.COOKIES['username']
        user_obj = User.objects.get(username=un)
        login(request,user_obj)
        return HttpResponseRedirect('/recruitment/dashboard')
    return render(request,'home.html')


def about(request):
    return render(request,'about.html')



def signup(request):
   # if ' username' in request.COOKIES:
       # un = request.COOKIES['username']
       # user_obj = User.objects.get(username=un)
        #login(request,user_obj)
        #return HttpResponseRedirect('/recruitment/dashboard')

    if request.method == 'POST':
        first =request.POST['first_name']
        last =request.POST['last_name']
        username = request.POST['username']
        mobile = request.POST['mobile']
        email = request.POST['email']
        password = request.POST['password']
        address = request.POST['address']


        user = User.objects.create_user(username,email,password)
        user.first_name = first
        user.last_name = last
        user.save()

        cust = register(user=user,phone_no=mobile,address=address)
        cust.save()
        return render(request,'signup.html',{'status':'Registred successfully'})


    return render(request,'signup.html')



def usLogin(request):
    if request.method == 'POST':
        if 'forgot' in request.POST:
            un = request.POST['uname']
            pwd = request.POST['confirmpd']
            user = User.objects.get(username=un)
            user.set_password(pwd)
            user.save()
            return render(request,'login.html',{'msz':'Password Changed Successfully!!'})    
        un =request.POST['username']
        pwd = request.POST['password']
        check = authenticate(username=un,password=pwd)
        if check:
            if check.is_staff:
                login(request,check)
                response = HttpResponseRedirect('/recruitment/compdashboard/')
                if 'rememberme' in request.POST:
                    response.set_cookie('username',un)
                    response.set_cookie('userid',check.id)
                    response.set_cookie('logintime',datetime.datetime.now())
                    return response
                else:
                    return response

            if check.is_active:
                login(request,check)
                response = HttpResponseRedirect('/recruitment/dashboard/')
                if 'rememberme' in request.POST:
                    response.set_cookie('username',un)
                    response.set_cookie('userid',check.id)
                    response.set_cookie('logintime',datetime.datetime.now())
                    return response
                else:
                    return response

           #return render(request,'login.html',{'msz':'Log in successfully'})
        else:
             return render(request,'login.html',{'msz':'invalid login details'})   
    return render(request,'login.html')


@login_required
def dashboard(request):
    log = User.objects.get(username=request.user)
    cust = register.objects.get(user=log)
    return render(request,'dashboard.html',{'customer':cust})

# to search jobs by user
@login_required
def getpost(request):
    if request.method == 'POST':
        p = request.POST['jb']
        q = request.POST['cy']
        all = postjob.objects.filter(job_title__contains=p)|postjob.objects.filter(job_location__contains=q)
        print(all)
        return render(request,'dashboard.html',{'all':all})
    else:
        return render(request,'dashboard.html',{'output1':'Error'})
    return render(request,'dashboard.html')





@login_required
def uslogout(request):

    logout(request)
    response = HttpResponseRedirect('/')
    response.delete_cookie('username')
    response.delete_cookie('logintime')
    response.delete_cookie('userid')
    return response


def companyView(request):
    if request.method =='POST':
        company_name = request.POST['cpname']
        mobile = request.POST['cpmobile']
        email = request.POST['email']
        username = request.POST['cpusername']
        cpassword = request.POST['password']
        address = request.POST['address']

        data1 = User.objects.create_user(username,email,cpassword)
        data1.save()
        data1.is_staff=True
        data1.save()

        comp = company(user1=data1,contact_no=mobile,address=address)
        comp.save()
        return render(request,'company.html',{'status':'Registred successfully'})


    return render(request,'company.html')


@login_required
def change_password(request):
    user_obj = User.objects.get(username=request.user.username)
    if request.method=='POST':
        old = request.POST['old']
        np = request.POST['new']
        match=check_password(old,user_obj.password)
        if match == True:
            user_obj.set_password(np)
            user_obj.save()
            return render(request,'changepassword.html',{'status':'Password Changed successfully!!!','col':'success'})
        else:
            return render(request,'changepassword.html',{'status1':'Incorrect current Password!!!','col':'danger'})    

    return render(request,'changepassword.html')

@login_required
def forgot_password(request):
    user_obj = User.objects.get(email=request.user.email)
    if request.method=='POST':
        old = request.POST.get('newpd')
        np = request.POST.get('confirmpd')
        match=check_password(old,user_obj.password)
        if match == True:
            user_obj.set_password(np)
            user_obj.save()
            return render(request,'login.html',{'status':'Password Changed successfully!!!','col':'success'})
        else:
            return render(request,'login.html',{'status1':'Incorrect current Password!!!','col':'danger'})    

    return render(request,'login.html')

@login_required
def change_profile(request):
    log = User.objects.get(username=request.user)
    reg = register.objects.get(user=log)
    if request.method == 'POST':
        con = request.POST['contact']
        add = request.POST['address']

        reg.phone_no = con
        reg.address = add
        reg.save()
        return render(request,'change_profile.html',{'re':reg})        
    return render(request,'change_profile.html',{'re':reg})

@login_required
def compdashboard(request):
    p = company.objects.get(user1__username=request.user.username)
    all = postjob.objects.filter(comp=p)
    return render(request,'compdashboard.html',{'customer':p,'all':all})

login_required
def candidatelist(request):
    # p = register.objects.get(user__username=request.user.username)
    ipl = apply.objects.filter(Comp1__user1__username=request.user.username)
    return render(request,'candid.html',{'all':ipl})


@login_required
def cpprofile(request):
    log = User.objects.get(username=request.user)
    cust = company.objects.get(user1=log)
    if request.method == 'POST':
        email = request.POST.get('emailid')
        con = request.POST.get('con')
        add = request.POST.get('address')

        cust.email = email
        cust.contact_no = con
        cust.address = add
        cust.save()
        return render(request,'compchangeprofile.html',{'company':cust,'status':'Changed Successfully !!'})
    return render(request,'compchangeprofile.html',{'company':cust})  

@login_required
def cppassword(request):
    p = User.objects.get(username=request.user.username)
    if request.method=='POST':
        old = request.POST['old']
        np = request.POST['new']
        match=check_password(old,p.password)
        if match == True:
            p.set_password(np)
            p.save()
            return render(request,'compchangepassword.html',{'status':'Password Changed successfully!!!','col':'success'})
        else:
            return render(request,'compchangepassword.html',{'status1':'Incorrect current Password!!!','col':'danger'})    

    return render(request,'compchangepassword.html')
    

@login_required
def pjobs(request):
    if request.method == 'POST':
        
        # if 'psubmit' in request.POST:
        company_name = request.POST['cname']
        hr_name = request.POST['hname']
        emailid = request.POST['cemail']
        contact = request.POST['cnumber']
        job_Title = request.POST['jtitle']
        job_cat = request.POST['jcat']
        Vacancies = request.POST['vacanacy']
        Qualification = request.POST['qual']
        job_loc = request.POST['loc']
        int_details = request.POST['itdetails']
        Candidate_exp = request.POST['cexp']
        Salary = request.POST['salary']
        Job_desc = request.POST['desc']
        
        p = company.objects.get(user1__username=request.user.username)
        

        post = postjob(comp=p,HR_name=hr_name,Email=emailid,contact_no=contact,job_title=job_Title,job_category=job_cat,vacancies=Vacancies,
        qualification=Qualification,job_location=job_loc,interview_details=int_details,candidate_exp=Candidate_exp,salary=Salary,job_desc=Job_desc)
        post.save()
        return HttpResponseRedirect('/recruitment/compdashboard')
    return render(request,'postjobs.html')

@login_required
def aform(request):
    uid = request.GET['uid']
    jid = request.GET['jid']
    cid = request.GET['cid']
    r = register.objects.get(user__id=uid)
    if request.method == 'POST':
        Qualification = request.POST['uqual']
        Experience = request.POST['uexp']
        grad_percentage = request.POST['gp']
        passing_yr = request.POST['py']
        desc_self = request.POST['oself']
        resume = request.FILES['uresume']
# apply.objects.filter(Comp1__user1__username=request.user.username)
        co = company.objects.get(id=cid)
        jo = postjob.objects.get(id=jid)
        ro = register.objects.get(user__username=request.user.username)
        que = apply(Comp1=co,job=jo,reg=ro,qualification=Qualification,Graduation_percentage=grad_percentage,passing_out_year=passing_yr,Resume=resume,experience=Experience,add_info=desc_self)
        que.save()
        #return render(request,'applyform.html',{'query':que,'status':'Submitted Successfully'})
        return HttpResponseRedirect('/recruitment/dashboard',{'status':'Submitted Successfully'})
   # else:
       # return render(request,'applyform.html',{'status':'Error'})
        

    return render(request,'applyform.html',{'rg':r.phone_no})

@login_required
def deleteView(request):
    if 'id' in request.GET:
        id_to_delete = request.GET['id']
        p = postjob.objects.get(id=id_to_delete)
        p.delete()
        return HttpResponseRedirect('/recruitment/compdashboard')

    return render(request,'compdashboard.html')    

@login_required
def deletecp(request):
    r = User.objects.get(username = request.user.username)
    r.delete()
    return HttpResponseRedirect('/')
   

@login_required
def deleteus(request):
    r = User.objects.get(username = request.user.username)
    r.delete()
    return HttpResponseRedirect('/')
 
@login_required
def can_delete(request):
    if 'id' in request.GET:
        cd = request.GET['id']
        d = apply.objects.get(id=cd)
        d.delete()
        return HttpResponseRedirect('/recruitment/compdashboard')

    return render(request,'candidt.html')    

  
 
def contactus(request):
    if request.method == 'POST':
       F_Name = request.POST['firstname']
       L_Name = request.POST['lastname']
       E_Mail = request.POST['ctemail']
       Mobile = request.POST['ctnumber']
       Msg = request.POST['ctmess']

       w = contact(first_name=F_Name,last_name=L_Name,Email_address=E_Mail,mobile_no=Mobile,Message=Msg)
       w.save()
       return render(request,'contact.html',{'status':'Message sent Successfully'})
    return render(request,'contact.html')


from django.http import HttpResponse
@login_required
def updatept(request):
    id_to_change = request.GET.get('ide')
    post = postjob.objects.get(id=id_to_change)
    if request.method == 'POST':
        hr_name = request.POST.get('HR_name')
        emailid = request.POST.get('emailid')
        contact = request.POST.get('contactnumber')
        job_Title = request.POST.get('jobtitle')
        job_cat = request.POST.get('jobcategory')
        Vacancies = request.POST.get('Vac')
        des = request.POST.get('desc')
        job_loc = request.POST.get('joblocation')
        Candidate_exp = request.POST.get('Exp')

        post.HR_name = hr_name
        post.Email = emailid
        post.contact_no = contact
        post.job_title = job_Title
        post.job_category = job_cat
        post.vacancies = Vacancies
        post.job_desc = des
        post.job_location = job_loc
        post.candidate_exp = Candidate_exp
        post.save()
        return render(request,'updatepost.html',{'postjob':post,'status':'Changed Successfully !!'})      
    return render(request,'updatepost.html',{'postjob':post})

@login_required
def sendmail(request):
    if request.method=='POST':
        # return HttpResponse(request.POST)
        if 'sendmail' in request.POST:
            t = request.POST['to'].split(',')
            sb = request.POST['subject']
            msz = request.POST['message']
           

            obj = EmailMessage(sb,msz,to=t)
            obj.send()
            return render(request,'compdashboard.html',{'status3':'Message Sent Successfully'})

        else:
            return HttpResponseRedirect('/recruitment/changeprofile/')    
    return render(request,'compdashboard.html',{'status':'Message sent Successfully'})

@login_required
def candid(request):
    return render(request,'candid.html')

@login_required
def smail(request):
    return render(request,'mail.html') 
   
import random
def forgot_pass(request):
    username = request.GET['uname']
    check = User.objects.filter(username=username)
    if len(check)>=1:
        em = User.objects.get(username=username)
        email = em.email
        otp = random.randint(10000,99999)
        msz = "Hii {} Here is Your OTP {}".format(username,otp)

        em = EmailMessage('OTP Verification',msz,to=[email,])
        em.send()
        return HttpResponse('An OTP Sent To Your Registred Email ID @'+str(otp))
    
    else:
        return HttpResponse(0)



#def uslogin(request):
#    return render(request,'login.html')

#def Home(request):
  #  return render(request,'home.html')
#def Logout(request):
  #  return HttpResponseRedirect('/abc')
    


    



